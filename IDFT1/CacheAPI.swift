//
//  CacheAPI.swift
//  IDFT1
//
//  Created by Wellyson Da cruz rocha on 23/01/2019.
//  Copyright © 2019 ipartts. All rights reserved.
//
import Foundation;
@objc public class CacheAPI: NSObject{
  
  
  @objc private var dataBaseInfo: Dictionary<String,Any> = [:];
  
  
  @objc public override init() {
    
   
  }
  
  @objc public func setDatabaseInfoKey(key: String,value: Any) -> Bool{
  
    self.dataBaseInfo[key] = value;
    return true;
  }
  
  @objc public func getDatabaseInfo()-> Dictionary<String,Any>{
    
    return self.dataBaseInfo;
  }
  
}
